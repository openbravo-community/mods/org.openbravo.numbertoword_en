/*
 *  *************************************************************************
 *  The contents of this file are subject to the Creative Commons 
 *  Attribution-ShareAlike 2.5 Spain License 
 *  (the "License"): http://creativecommons.org/licenses/by-sa/2.5/es/. 
 *  Software distributed under the License is  distributed  on  an "AS IS" 
 *  basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *  See the License for the specific  language  governing  rights  
 *  and  limitations under the License.
 *  This is a packaging in a module done by Openbravo SLU of the code snippet
 *  "POS - Number To Words In Ticket (Spanish)" 
 *  by Macumbero and Zil__, published on Openbravo wiki, 
 *  http://wiki.openbravo.com/wiki/POS_-_Number_To_Words_In_Ticket_(Spanish) 
 *  and used under the License.
 *  *************************************************************************
 */

package org.openbravo.numbertoword_en.erpCommon.utility;

import java.math.BigDecimal;

import org.openbravo.numbertoword.erpCommon.utility.NumberToWord;

public class NumberToWord_en extends NumberToWord {
  private static String[] _groups = { "", "million", "billion", "trillion" };

  private static String[] _units = { "", "one", "two", "three", "four", "five", "six", "seven",
      "eight", "nine" };

  private static String[] _ten1 = { "", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
      "sixteen", "seventeen", "eighteen", "nineteen" };

  private static String[] _tens = { "", "ten", "twenty", "thirty", "forty", "fifty", "sixty",
      "seventy", "eighty", "ninety" };

  private static String[] _hundreds = { "", "one hundred", "two hundred", "three hundred",
      "four hundred", "five hundred", "six hundred", "seven hundred", "eight hundred",
      "nine hundred" };

  public static String thousandText(int num) {
    int n = num;
    if (n == 0) {
      return "";
    }

    int hundreds = n / 100;
    n = n % 100;
    int tens = n / 10;
    int units = n % 10;

    String suffix = "";

    if (tens == 0 && units != 0) {
      suffix = _units[units];
    }

    if (tens == 1 && units != 0) {
      suffix = _ten1[units];
    }

    if (tens == 2 && units != 0) {
      suffix = "twenty" + _units[units];
    }

    if (units == 0) {
      suffix = _tens[tens];
    }

    if (tens > 2 && units != 0) {
      suffix = _tens[tens] + " " + _units[units];
    }

    if (hundreds != 0) {
      return _hundreds[hundreds] + " " + suffix;
    }

    if (units == 0 && tens == 0) {
      return "hundred";
    }
    if (hundreds == 0) {
      return suffix;
    }
    return "hundred " + suffix;
  }

  public static String toWord(long num) {
    String result = "";
    int group = 0;
    long n = num;
    while (n != 0 && group < _groups.length) {
      long fragment = n % 1000000;
      int millarAlto = (int) (fragment / 1000);
      int underathousand = (int) (fragment % 1000);
      n = n / 1000000;

      String groupname = _groups[group];
      if (fragment > 1 && group > 0) {
        groupname += "s";
      }

      if ((millarAlto != 0) || (underathousand != 0)) {
        if (millarAlto > 1) {
          result = thousandText(millarAlto) + " thousand " + thousandText(underathousand) + " "
              + groupname + " " + result;
        }

        if (millarAlto == 0) {
          result = thousandText(underathousand) + " " + groupname + " " + result;
        }

        if (millarAlto == 1) {
          result = "one thousand " + thousandText(underathousand) + " " + groupname + " " + result;
        }
      }
      group++;
    }
    result = result.trim().concat(" ");
    return result;
  }

  @Override
  public String convert(BigDecimal number) {

    double num = number.doubleValue();

    num = (double) (Math.round(num * Math.pow(10, 2)) / Math.pow(10, 2));
    long number_whole = (long) num;
    int number_decimal = (int) ((num * 100) - (number_whole * 100));
    String value;
    if (number_decimal == 0) {
      value = toWord(number_whole).concat("and 00/100");
    } else {
      value = toWord(number_whole).concat("and ")
          .concat(Integer.toString(number_decimal))
          .concat("/100");
    }
    value = value.substring(0, 1).toUpperCase().concat(value.substring(1));
    return value;
  }

}
